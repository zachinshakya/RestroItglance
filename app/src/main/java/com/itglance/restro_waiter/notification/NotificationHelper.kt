package com.itglance.restro_waiter.notification
import android.app.Application
import android.content.Context
import org.json.JSONObject
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONException
import android.view.View
import androidx.appcompat.view.menu.MenuView
import com.google.firebase.messaging.FirebaseMessaging
import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.main.MenuViewModel
import com.itglance.restro_waiter.main.menu.order.Order
import com.itglance.restro_waiter.main.orders.ViewHolder
import com.itglance.restro_waiter.main.tables.Group
import kotlinx.android.synthetic.main.all_order_recycler_view_layout_design.*
import kotlinx.android.synthetic.main.all_order_recycler_view_layout_design.view.*


class NotificationHelper(context: Context):AppCompatActivity(){

    //for notification
    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val serverKey =
            "key=" + "AIzaSyAZbKGUTBrBAddQOmfgHciZfI8Xq-xWDIw"
    private val contentType = "application/json"
    private val TOPIC = "restro_waiter"

    private val requestQueue: RequestQueue by lazy {
            Volley.newRequestQueue(context.applicationContext)
    }

    fun subsribeToTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC)
                .addOnCompleteListener { task ->
                    var msg = "Successfully Subscribed!!!"
                    if (!task.isSuccessful) {
                        msg = "Failed To Subscribed!!!"
                    }
                    Log.d("TAG", msg)
                }
    }

    fun sendNotification(view: View) {
        val order = view.tag as Order
        var itemName=order.itemName
        val menuViewModel=MenuViewModel(application = Application())
        val groupObject=menuViewModel.getGroupObject(order.group)
        val tableName=groupObject?.name
        val topic = "/topics/$TOPIC" //topic has to match what the receiver subscribed to
        val notification = JSONObject()
        val notificationBody = JSONObject()
        try {
            notificationBody.put("title", "Restro-Bytes Notification")
            notificationBody.put("message",itemName+" "+tableName)
            notification.put("to", topic)
            notification.put("data", notificationBody)
            Log.e("TAG", "try")
        } catch (e: JSONException) {
            Log.e("TAG", "onCreate: " + e.message)
        }
        val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
                Response.Listener<JSONObject> { response ->
                    Log.i("TAG", "onResponse: $response")
                },
                Response.ErrorListener {
                   //Toast.makeText(this, "Request error", Toast.LENGTH_LONG).show()
                    Log.i("TAG", "onErrorResponse: Didn't work")
                }) {

            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] = serverKey
                params["Content-Type"] = contentType
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }

}