package com.itglance.restro_waiter.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {

    public static boolean hasNetwork(Context context) {
        ConnectivityManager connectionManager=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectionManager.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
           return true;
        } else {

            return false;
        }
    }
}
