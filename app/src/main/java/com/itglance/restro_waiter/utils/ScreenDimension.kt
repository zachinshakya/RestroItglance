package com.itglance.restro_waiter.utils

import android.content.Context
import android.content.res.Resources
import android.view.View
import com.itglance.restro_waiter.R

class ScreenDimension {

    companion object {
        private var appLogoTranslateTop: Float = 0.0f
        private var creditContainerTranslateInitial: Float = 0.0f

        fun getHeight(): Long {
            return Resources.getSystem().displayMetrics.heightPixels.toLong()
        }

        fun getWidth(): Long {
            return Resources.getSystem().displayMetrics.widthPixels.toLong()
        }

        fun getHeightOf(view: View): Long {
            view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
            return view.measuredHeight.toLong()
        }

        fun getWidthOf(view: View): Long {
            view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
            return view.measuredWidth.toLong()
        }

        fun calculateLoginFormCardTranslateTop(context: Context): Float {
            return getHeight() / 3 - context.resources.getDimension(R.dimen.app_logo) / 4
        }

        fun getAppLogoTranslateTop(): Float {
            return appLogoTranslateTop
        }

        fun setAppLogoTranslateTop(appLogoTranslateTop: Float) {
            ScreenDimension.appLogoTranslateTop = appLogoTranslateTop
        }

        fun getCreditContainerTranslateInitial(): Float {
            return creditContainerTranslateInitial
        }

        fun setCreditContainerTranslateInitial(creditContainerTranslateInitial: Float) {
            ScreenDimension.creditContainerTranslateInitial = creditContainerTranslateInitial
        }
    }
}