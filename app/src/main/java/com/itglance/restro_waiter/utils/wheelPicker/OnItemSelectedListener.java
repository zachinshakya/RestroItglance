
package com.itglance.restro_waiter.utils.wheelPicker;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
