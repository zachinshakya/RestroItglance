package com.itglance.restro_waiter.utils

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.view.View
import android.view.inputmethod.InputMethodManager

class KeyboardUtils {
    companion object {
        private var inputMethodManager: InputMethodManager? = null

        fun hideKeyboard(activity: Activity) {
            inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (inputMethodManager != null) {
                inputMethodManager!!.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }
        }

        fun showKeyboard(activity: Activity) {
            inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (inputMethodManager != null) {
                inputMethodManager!!.toggleSoftInputFromWindow(activity.currentFocus!!.windowToken, InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY)
            }
        }

        fun getIsKeyboardShown(rootView: View): Boolean {
            /*
         * 128dp = 32dp * 4, minimum button height 32dp and generic 4 rows soft
         * keyboard
         */
            val SOFT_KEYBOARD_HEIGHT_DP_THRESHOLD = 128

            val r = Rect()
            rootView.getWindowVisibleDisplayFrame(r)
            val dm = rootView.resources.displayMetrics
            /*
         * heightDiff = rootView height - status bar height (r.top) - visible
         * frame height (r.bottom - r.top)
         */
            val heightDiff = rootView.bottom - r.bottom
            /* Threshold size: dp to pixels, multiply with display density */

            return heightDiff > SOFT_KEYBOARD_HEIGHT_DP_THRESHOLD * dm.density
        }

    }
}