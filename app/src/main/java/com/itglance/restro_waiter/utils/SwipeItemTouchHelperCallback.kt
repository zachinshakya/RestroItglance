package com.itglance.restro_waiter.utils

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.View
import androidx.appcompat.view.menu.MenuView
import androidx.core.content.ContextCompat

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.itglance.restro_waiter.R


class SwipeItemTouchHelperCallback private constructor(builder: Builder) : ItemTouchHelper.SimpleCallback(builder.dragDirs, builder.swipeDirs) {

    private var onItemSwipeLeftListener = builder.onItemSwipeLeftListener
    private var onItemSwipeRightListener = builder.onItemSwipeRightListener
    private var swipeEnabled = builder.swipeEnabled

    private var icon: Drawable? = null
    private val background: ColorDrawable? = null


    override fun isItemViewSwipeEnabled(): Boolean {
        return swipeEnabled
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        if (direction == ItemTouchHelper.LEFT) {
//            icon = Drawable(R.drawable.ic_add)

            onItemSwipeLeftListener!!.onItemSwiped(viewHolder.itemView)
        } else if (direction == ItemTouchHelper.RIGHT) {
            onItemSwipeRightListener!!.onItemSwiped(viewHolder.itemView)
        }
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            val itemView = viewHolder.itemView
            val backgroundCornerOffset = 20 //so background is behind the rounded corners of itemView


            when {
                dX < 0 -> { // Swiping to the right

                     val background = ColorDrawable(Color.RED)

                    background.setBounds(itemView.left, itemView.top,
                            itemView.left + dX.toInt() + backgroundCornerOffset, itemView.bottom)

                    background.draw(c)

                }
                dX > 0 -> {
                    val background = ColorDrawable(Color.BLUE)

                    background.setBounds(itemView.left, itemView.top,
                            itemView.left + dX.toInt() + backgroundCornerOffset, itemView.bottom)
                    background.draw(c)
                }
                else -> { // view is unSwiped
    //                background.setBounds(0, 0, 0, 0)
                }
            }

        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    interface OnItemSwipeListener {
        fun onItemSwiped(view: View)
    }

    class Builder(val dragDirs: Int, val swipeDirs: Int) {
        var onItemSwipeLeftListener: OnItemSwipeListener? = null
        var onItemSwipeRightListener: OnItemSwipeListener? = null
        var swipeRightBackgroundColor: Int = 0
        var swipeLeftBackgroundColor: Int = 0

        var swipeEnabled: Boolean = false

            private set

        fun onItemSwipeLeftListener(`val`: OnItemSwipeListener): Builder {
            onItemSwipeLeftListener = `val`
            return this
        }

        fun onItemSwipeRightListener(`val`: OnItemSwipeListener): Builder {
            onItemSwipeRightListener = `val`
            return this
        }

        fun setSwipeEnabled(`val`: Boolean): Builder {
            swipeEnabled = `val`
            return this
        }
        fun addSwipeRightBackgroundColor(`val`: Int): Builder {
            swipeRightBackgroundColor = `val`
            return this
        }

        fun addSwipeLeftBackgroundColor(`val`: Int): Builder {
            swipeLeftBackgroundColor = `val`
            return this
        }

        fun build(): SwipeItemTouchHelperCallback {
            return SwipeItemTouchHelperCallback(this)
        }
    }
}