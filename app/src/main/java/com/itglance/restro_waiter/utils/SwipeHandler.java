package com.itglance.restro_waiter.utils;

import android.view.View;

public interface SwipeHandler {

	void onItemSwipedLeft(View view);

	void onItemSwipedRight(View view);
}
