package com.itglance.restro_waiter.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.itglance.restro_waiter.R;
import com.itglance.restro_waiter.main.menu.MenuFragment;

public class AddMenuItemDialog extends Dialog {
    private Context context;
    private Dialog dialog;
    private MenuFragment menuFragment;
    private String categoryId;


    public AddMenuItemDialog(Context context, String categoryId) {
        super(context);
        this.context = context;
        this.categoryId = categoryId;
        init();
    }

    private void init() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.add_menu_item_layout, null);
        EditText itemNameEditText = v.findViewById(R.id.add_menu_item_name);
        EditText itemPriceEditText = v.findViewById(R.id.add_menu_item_price);
        builder.setView(v);
        builder.setCancelable(false).setPositiveButton("OK", (dialog, id) ->
                {
                    String itemName = itemNameEditText.getText().toString();
                        try{
                            Double itemPrice = Double.parseDouble(itemPriceEditText.getText().toString());
                            menuFragment = new MenuFragment();
                            menuFragment.addItem(itemName, itemPrice, "Large", categoryId);
                        }catch (NumberFormatException e){
                            Toast.makeText(context,"Enter valid Item",Toast.LENGTH_LONG).show();
                        }catch (NullPointerException e){
                            Toast.makeText(context,"Enter valid Item",Toast.LENGTH_LONG).show();
                        }
                })
                .setNegativeButton("Cancel", (dialog, id) ->
                        dismiss());
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setTitle("Add Menu Item");


    }

    public void show() {
        if (dialog != null) {
            dialog.show();
        }
    }

    public void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
