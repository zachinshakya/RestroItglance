package com.itglance.restro_waiter.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.itglance.restro_waiter.R;

public class BillDiscountDialog extends Dialog {

    private Context context;
    private Dialog dialog;
    private int discountedAmount;

    public BillDiscountDialog(Context context, int discountedAmount) {
        super(context);
        this.context = context;
        this.discountedAmount = discountedAmount;

        init();
    }

    private void init() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.confirm_discount_dialog_layout, null);
        TextView discountedAmountTextView =  v.findViewById(R.id.discountText);


        discountedAmountTextView.setText(((String.valueOf(discountedAmount).substring(1))));

        builder.setView(v);
        builder.setCancelable(false).setPositiveButton("OK", (dialog, id) ->

                Toast.makeText(context, "Bill Paid", Toast.LENGTH_SHORT).show())
                .setNegativeButton("Cancel",(dialog, id) ->
                        dismiss());
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setTitle("Confirm Discount");
    }

    public void show() {
        if (dialog != null) {
            dialog.show();
        }
    }

    public void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
