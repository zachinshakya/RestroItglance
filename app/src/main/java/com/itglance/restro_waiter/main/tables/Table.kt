package com.itglance.restro_waiter.main.tables

import java.util.*

class Table {
    var id: String = UUID.randomUUID().toString()
    lateinit var name: String
    lateinit var category: String
    lateinit var status: Status

    @SuppressWarnings("unused")
    constructor()

    constructor(name: String, category: String, status: Status){
        this.name = name
        this.category = category
        this.status = status
    }
}
