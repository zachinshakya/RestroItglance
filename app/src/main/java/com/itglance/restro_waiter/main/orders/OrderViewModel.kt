package com.itglance.restro_waiter.main.orders

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import com.couchbase.lite.CouchbaseLiteException
import com.couchbase.lite.ResultSet
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.itglance.restro_waiter.DatabaseManager
import com.itglance.restro_waiter.Queries
import com.itglance.restro_waiter.main.MenuViewModel
import com.itglance.restro_waiter.main.menu.order.Order
import com.itglance.restro_waiter.main.tables.Table

class OrderViewModel  @Throws(CouchbaseLiteException::class)
constructor(application: Application) : AndroidViewModel(application) {
    val orders = MutableLiveData<List<Order>>()
    private val objectMapper = ObjectMapper()

    private val allOrders = ArrayList<Order>()

    init {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        val queries = Queries.getInstance(application)
        val orderListQuery = queries.getOrders()
        orderListQuery.addChangeListener { change -> postOrders(change.results) }
        postOrders(orderListQuery.execute())
    }

    private fun postOrders(results: ResultSet) {
        allOrders.clear()
        for (categoryMap in results.allResults()) {
            val valueMap = categoryMap.getDictionary(DatabaseManager.getSharedInstance(getApplication()).database.name)
            val order = objectMapper.convertValue(valueMap.toMap(), Order::class.java)
            allOrders.add(order)
        }
        orders.postValue(allOrders)
    }
}