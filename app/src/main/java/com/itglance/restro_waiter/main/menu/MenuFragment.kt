package com.itglance.restro_waiter.main.menu

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.TextWatcher
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextSwitcher
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ItemTouchHelper
import com.couchbase.lite.CouchbaseLiteException
import com.couchbase.lite.MutableDocument
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.itglance.restro_waiter.DatabaseManager

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.RecyclerViewItemOffsetDecoration
import com.itglance.restro_waiter.dialog.AddMenuItemDialog
import com.itglance.restro_waiter.main.MenuViewModel
import com.itglance.restro_waiter.main.menu.category.CategoryAdapter
import com.itglance.restro_waiter.main.menu.item.ItemAdapter
import com.itglance.restro_waiter.databinding.FragmentMenuBinding
import com.itglance.restro_waiter.main.menu.category.Category
import com.itglance.restro_waiter.main.menu.item.Item
import com.itglance.restro_waiter.main.menu.order.Order
import com.itglance.restro_waiter.utils.SwipeHandler
import com.itglance.restro_waiter.main.menu.order.OrderAdapter
import com.itglance.restro_waiter.main.tables.Table
import com.itglance.restro_waiter.utils.SwipeItemTouchHelperCallback
import java.util.ArrayList
import java.util.HashMap


class MenuFragment : androidx.fragment.app.Fragment() {

    private lateinit var menuItemView: RecyclerView
    private lateinit var categoryView: RecyclerView
    private lateinit var orderView: RecyclerView
    private lateinit var menu: MenuViewModel
    private lateinit var addMenuFloatingActionButton: FloatingActionButton
    private lateinit var addCategoryFloatingActionButton: FloatingActionButton
    private lateinit var binding : FragmentMenuBinding
    private lateinit var addCategoryEditText: EditText
    private lateinit var categoryId:String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_menu, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        menuItemView = view.findViewById(R.id.menu_item_recycler_view)
        categoryView = view.findViewById(R.id.menu_category_recycler_view)
        orderView = view.findViewById(R.id.menu_order_recycler_view)
        addMenuFloatingActionButton = view.findViewById(R.id.add_menu_floating_button)
        addCategoryFloatingActionButton = view.findViewById(R.id.add_category_floating_button)
        addCategoryEditText = view.findViewById(R.id.add_category_edittext)



        addMenuFloatingActionButton.setOnClickListener {
            val addMenuItemDialog = AddMenuItemDialog(context,categoryId)
            addMenuItemDialog.show()
        }





        addCategoryEditText.setOnEditorActionListener {_,_,_ ->
            onCategorySelected(false)
            onCategoryEditTextSelected(true)
            false
        }


        addCategoryFloatingActionButton.setOnClickListener{
            addCategory(addCategoryEditText.text.toString())
            addCategoryEditText.text = null
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        menu = ViewModelProviders.of(activity!!).get(MenuViewModel::class.java)
        menu.currentTable.observe(this, Observer { menu.updateGroupOrders() })
        binding.handler = OrderViewSwipeHandler(menu)
        setupMenuItems()
        setupCategory()
        setupOrders()

    }
    private fun addCategory(category:String){
        if (category != ""){
            val menuCategories = ArrayList<Category>()

            val categoryItem = Category(category)
            menuCategories.add(categoryItem)

            val application = AppCompatActivity()
            val dbmgr = DatabaseManager.getSharedInstance(application)
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

            for (category in menuCategories) {
                try {
                    val categoryMaps = MutableDocument(objectMapper.convertValue(category, HashMap::class.java) as HashMap<String, Any>)
                    categoryMaps.setString("type", "category")
                    dbmgr.database.save(categoryMaps)
                } catch (e: CouchbaseLiteException) {
                    e.printStackTrace()
                }

            }
        }
        else{
            Toast.makeText(context,"Inter Category name", Toast.LENGTH_SHORT).show()
        }
    }
    fun addItem(itemName:String,price:Double,size:String,category: String){
        if (itemName != ""){
            val menuItems = ArrayList<Item>()

            val item = Item(itemName,price,size,category)
            menuItems.add(item)

            val application = AppCompatActivity()
            val dbmgr = DatabaseManager.getSharedInstance(application)
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

            for (menuItem in menuItems) {
                try {
                    val menuItemDocs = MutableDocument(objectMapper.convertValue(menuItem, HashMap::class.java) as HashMap<String, Any>)
                    menuItemDocs.setString("type", "menu_item")
                    dbmgr.database.save(menuItemDocs)
                } catch (e: CouchbaseLiteException) {
                    e.printStackTrace()
                }
            }
        }
        else{
            Toast.makeText(context,"Enter Items", Toast.LENGTH_SHORT).show()
        }
    }
    private fun setupMenuItems() {
        menuItemView.setHasFixedSize(true)
        context?.let {
            val adapter = ItemAdapter(it)

            (menuItemView.layoutManager as androidx.recyclerview.widget.GridLayoutManager).spanSizeLookup = (object : androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position != adapter.expandedPosition) {
                        1
                    } else {
                        2
                    }
                }
            })
            menu.menuItems.observe(this, Observer(adapter::submitList))
            val recyclerViewItemOffsetDecoration = RecyclerViewItemOffsetDecoration(it, R.dimen.recycler_view_item_offset)
            menuItemView.addItemDecoration(recyclerViewItemOffsetDecoration)
            menuItemView.adapter = adapter
        }
    }

    private fun setupCategory() {
        context?.let {
            val adapter = CategoryAdapter(it)
            menu.categories.observe(this, Observer(adapter::submitList))
            adapter.currentCategory.observe(this, Observer(menu::setMenuCategory))
            categoryView.adapter = adapter
            adapter.bottomReached.observe(this, Observer { visible -> onBottomReached(visible!!) })
            adapter.currentCategory.observe(this,Observer{ current -> categoryId = current.id} )
            adapter.categorySelected.observe(this, Observer { visibleFAB -> onCategorySelected(visibleFAB!!) })
            adapter.categorySelected.observe(this, Observer {onCategoryEditTextSelected(false)})
        }
    }
    private fun onCategoryEditTextSelected(isSelected: Boolean){
        if(isSelected){
            addCategoryFloatingActionButton.show()
        }
        else{
            addCategoryFloatingActionButton.hide()
        }

    }

    private fun onCategorySelected(isSelected: Boolean) {
        if (isSelected)
            addMenuFloatingActionButton.show()
        else
            addMenuFloatingActionButton.hide()
    }

    private fun onBottomReached(isVisible: Boolean) {
        if (isVisible) {
            addCategoryEditText.visibility = View.VISIBLE
            addCategoryEditText.isEnabled = true
        } else {
            addCategoryEditText.visibility = View.GONE
            addCategoryEditText.isEnabled = false
        }
    }

    private fun setupOrders() {
        context?.let {
            val adapter = OrderAdapter(it)



//            menu.allOrders.observe(this, Observer { orders ->
//                adapter.submitList(orders!!.filter { order -> order.status != Status.CLOSE.name }.sortedBy { order -> order.time }.reversed())
//            })

            adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    if (positionStart == 0) {
                        orderView.layoutManager?.scrollToPosition(0)
                    }
                }
            })
            menu.orders.observe(this, Observer(adapter::submitList))
            orderView.adapter = adapter
        }
    }

    class OrderViewSwipeHandler(var menu: MenuViewModel) : SwipeHandler {
        override fun onItemSwipedLeft(view: View) {
            menu.removeOrder(view.tag as Order)
        }

        override fun onItemSwipedRight(view: View) {
            menu.setOrderAsServed(view.tag as Order)
        }
    }

    companion object {
        private val TAG = "MenuFragment"
    }
}
