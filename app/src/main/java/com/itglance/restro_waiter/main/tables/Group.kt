package com.itglance.restro_waiter.main.tables

import java.util.UUID

class Group() {
    var id = UUID.randomUUID().toString()
    lateinit var name: String
    var table: String? = null
    val time = System.currentTimeMillis()
}
