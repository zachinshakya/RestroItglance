package com.itglance.restro_waiter.main.menu.item

import androidx.lifecycle.MutableLiveData
import android.content.Context
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.common.ChoiceListener

class ItemAdapter(private val context: Context) : ListAdapter<Item, ItemViewHolder>(DIFF_CALLBACK) {

    lateinit var expanded: Item
    var expandedPosition: Int = -1
    lateinit var listner: ChoiceListener<Item>
    var boottom = MutableLiveData<Boolean>()


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.menu_item_recycler_view_layout, viewGroup, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ItemViewHolder, position: Int) {
        viewHolder.bind(getItem(position))
        viewHolder.itemView.tag = getItem(position)

        viewHolder.itemView.setOnLongClickListener {
           expand(viewHolder, position)
        }

        if (position == itemCount - 1 && (boottom.value == null || !boottom.value!!)) {
            boottom.value = true
            boottom.postValue(true)
        } else if (boottom.value == null || boottom.value!!) {
            boottom.value = true
            boottom.postValue(false)
        }

        if (!::expanded.isInitialized || getItem(position) != expanded) {
            viewHolder.actionLayout.visibility = View.GONE
        } else {
            viewHolder.actionLayout.visibility = View.VISIBLE
        }
    }

    private fun expand(itemViewHolder: ItemViewHolder, position: Int): Boolean {
        notifyItemChanged(expandedPosition)
        expandedPosition = itemViewHolder.adapterPosition
        expanded = getItem(position)
        notifyItemChanged(expandedPosition)
        return true
    }



    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Item>() {
            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.name == newItem.name && oldItem.size == newItem.size && oldItem.price == newItem.price
            }
        }
    }
}
