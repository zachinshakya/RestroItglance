package com.itglance.restro_waiter.main.menu.order

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.main.MenuViewModel
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeUnit

class OrderViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
    var name: TextView = itemView.findViewById(R.id.ordered_item_name_textView)
    var quantity: TextView = itemView.findViewById(R.id.ordered_item_quantity_textView)
    var duration: TextView = itemView.findViewById(R.id.ordered_time_textView)
    var cardView :CardView = itemView.findViewById(R.id.order_card_view)
    var status: TextView = itemView.findViewById(R.id.order_status)

    fun bind(order: Order) {
        name.text = order.itemName
        quantity.text = order.quantity.toString()
        duration.text = TimeUnit.MILLISECONDS.toMinutes((System.currentTimeMillis() - order.time)).toString() + "m"
        duration.setTextColor(Color.RED)
        status.text = order.status
    }
}
