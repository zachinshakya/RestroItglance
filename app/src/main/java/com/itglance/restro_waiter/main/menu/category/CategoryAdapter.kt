package com.itglance.restro_waiter.main.menu.category

import androidx.lifecycle.MutableLiveData
import android.content.Context
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.common.ItemSelectedListener

class CategoryAdapter(private val context: Context) : ListAdapter<Category, CategoryViewHolder>(DIFF_CALLBACK) {

    private var selected = -1

    val currentCategory = MutableLiveData<Category>()
    var categorySelected = MutableLiveData<Boolean>()
    var bottomReached = MutableLiveData<Boolean>()

    init {
        bottomReached.value = false
        categorySelected.value = false
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CategoryViewHolder {

        val view = LayoutInflater.from(context).inflate(R.layout.menu_category_recycler_view_layout, viewGroup, false)
        return CategoryViewHolder(view)
    }

    private fun setCurrentCategory(adapterPosition: Int) {
        //set value so that appropriate values are read when setting selected
        currentCategory.value = getItem(adapterPosition)
        currentCategory.postValue(getItem(adapterPosition))
        notifyItemChanged(adapterPosition)
        notifyItemChanged(selected)
        selected = adapterPosition
        if (selected==-1)
            categorySelected.postValue(false)
        else
            categorySelected.postValue(true)
    }

    override fun onBindViewHolder(categoryViewHolder: CategoryViewHolder, position: Int) {
        categoryViewHolder.bind(getItem(position), ItemSelectedListener(this::setCurrentCategory), currentCategory.value)
        if (position == itemCount-1 && (bottomReached.value == null || !bottomReached.value!!))
            bottomReached.postValue(true)
        else if (bottomReached.value == null || bottomReached.value!!){
            bottomReached.postValue(false)
        }

    }

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Category>() {
            override fun areItemsTheSame(oldMenuItem: Category, newMenuItem: Category): Boolean {
                return oldMenuItem.id == newMenuItem.id
            }

            override fun areContentsTheSame(oldMenuItem: Category, newMenuItem: Category): Boolean {
                return oldMenuItem.name == newMenuItem.name
            }
        }
    }

}
