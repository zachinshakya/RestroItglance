package com.itglance.restro_waiter.main.menu.item

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView

import com.itglance.restro_waiter.R
import android.widget.LinearLayout

class ItemViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
    var name: TextView = itemView.findViewById(R.id.menu_item_name_textView)
    var price: TextView = itemView.findViewById(R.id.menu_item_price)

    var size: TextView? = null

    var actionLayout : LinearLayout = itemView.findViewById(R.id.action_content_layout_container)

    private val quantity: TextView = itemView.findViewById(R.id.menu_item_order_number_identifier)

    fun bind(item: Item) {
        name.text = item.name
        price.text = item.price.toString()

    }


}
