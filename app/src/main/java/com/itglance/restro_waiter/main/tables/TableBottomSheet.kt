package com.itglance.restro_waiter.main.tables

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.opengl.Visibility
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.main.MainActivity
import com.itglance.restro_waiter.main.MenuViewModel
import java.util.*
import androidx.annotation.NonNull
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
import android.view.View
import android.widget.Toast


class TableBottomSheet(activity: MainActivity) {

    private val drawerLayout: androidx.drawerlayout.widget.DrawerLayout = activity.findViewById(R.id.main_activity_drawer_layout)
    private val context: Context = drawerLayout.context
    private val bottomSheet: ConstraintLayout = drawerLayout.findViewById(R.id.table_bottom_sheet_view)
    private val draggingView: View = drawerLayout.findViewById(R.id.selected_table_bar)
    private val tables: androidx.recyclerview.widget.RecyclerView = drawerLayout.findViewById(R.id.table_recycler_view)
    private val category: androidx.recyclerview.widget.RecyclerView = drawerLayout.findViewById(R.id.table_category_recycler_view)
    private val selectedTable: TextView = drawerLayout.findViewById(R.id.selected_table_name)
    val menu: MenuViewModel = ViewModelProviders.of(activity).get(MenuViewModel::class.java)

    init {
        menu.currentTable.observe(activity, Observer{ current -> selectedTable.text = current?.name})
        setupTables(activity)
        setupCategories(activity)
        setupView()
        menu.currentTableCategory.observe(activity, Observer(menu::setCurrentTableCategory))
    }

    private fun setupView() {
        val sheetBehavior = BottomSheetBehavior.from(bottomSheet)
        sheetBehavior.isHideable = false
        sheetBehavior.setBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (sheetBehavior.state== STATE_COLLAPSED)
                    draggingView.visibility = View.VISIBLE


            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // React to dragging events

//                Toast.makeText(context,"Dragging",Toast.LENGTH_SHORT).show()
                draggingView.visibility = View.GONE
            }
        })
    }

    private fun setupTables(activity: MainActivity) {
        val adapter = TableAdapter(context)
        menu.tables.observe(activity, Observer(adapter::submitList))
        adapter.current.value = menu.currentTable.value
        tables.adapter = adapter
        adapter.current.observe(activity, Observer{ table -> menu.currentTable.postValue(table)})
    }

    private fun setupCategories(activity: MainActivity) {
        val adapter = CategoryAdapter(context)
        menu.tableCategories.observe(activity, Observer(adapter::submitList))
        adapter.current = menu.currentTableCategory
        category.adapter = adapter
    }
}
