package com.itglance.restro_waiter.main.tables

import androidx.lifecycle.MutableLiveData
import android.content.Context
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.common.ItemSelectedListener

class TableAdapter(private val tableBottomSheet: Context) : ListAdapter<Table, TableViewHolder>(DIFF_CALLBACK) {

    var current: MutableLiveData<Table> = MutableLiveData()

    private var selected = -1

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): TableViewHolder {
        val view = LayoutInflater.from(tableBottomSheet).inflate(R.layout.table_recycler_view_layout, viewGroup, false)
        return TableViewHolder(view)
    }

    private fun setCurrent(adapterPosition: Int) {
        current.value = getItem(adapterPosition)
        current.postValue(getItem(adapterPosition))
        notifyItemChanged(selected)
        notifyItemChanged(adapterPosition)
        selected = adapterPosition
    }

    override fun onBindViewHolder(viewHolder: TableViewHolder, position: Int) {
        viewHolder.bind(getItem(position), ItemSelectedListener { adapterPosition -> setCurrent(adapterPosition) })
        viewHolder.setAsSelected(getItem(position), current.value)
    }

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Table>() {
            override fun areItemsTheSame(oldItem: Table, newItem: Table): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Table, newItem: Table): Boolean {
                return oldItem.name == newItem.name
            }
        }
    }
}
