package com.itglance.restro_waiter.main.tables

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.common.ItemSelectedListener

class TableViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
    var name: TextView = itemView.findViewById(R.id.table_name)

    fun bind(category: Table, setCurrent: ItemSelectedListener) {
        name.text = category.name
        itemView.setOnClickListener {
            setCurrent(adapterPosition)
        }
    }

    fun setAsSelected(table: Table, current: Table?) {
        current?.let {
            if (it.id == table.id) {
                itemView.background = itemView.resources.getDrawable(R.drawable.table_category_pressed)
            }
            else {
                itemView.background = itemView.resources.getDrawable(R.drawable.table_category_enable)
            }
        }
    }
}
