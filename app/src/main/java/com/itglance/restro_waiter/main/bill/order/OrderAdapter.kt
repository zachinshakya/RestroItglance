package com.itglance.restro_waiter.main.bill.order

import android.content.Context
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.main.menu.order.Order

class OrderAdapter(private val context: Context) : ListAdapter<Order, OrderViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): OrderViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.bill_order_list, viewGroup, false)
        return OrderViewHolder(view)
    }

    override fun onBindViewHolder(orderViewHolder: OrderViewHolder, position: Int) {
        orderViewHolder.bind(getItem(position))
        orderViewHolder.itemView.tag = getItem(position)
    }

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Order>() {
            override fun areItemsTheSame(oldMenuItem: Order, newMenuItem: Order): Boolean {
                return oldMenuItem.id == newMenuItem.id
            }

            override fun areContentsTheSame(oldMenuItem: Order, newMenuItem: Order): Boolean {
                return oldMenuItem.description == newMenuItem.description && oldMenuItem.item == newMenuItem.item && oldMenuItem.quantity == newMenuItem.quantity
            }
        }
    }
}