package com.itglance.restro_waiter.main.tables

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView

import com.itglance.restro_waiter.R

class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var name: TextView = itemView.findViewById(R.id.table_category_text_view)

    fun setAsSelected(table: TableCategory, current: TableCategory?) {
        current?.let {
            if (it.id == table.id) {
                itemView.background = itemView.resources.getDrawable(R.drawable.table_category_pressed)
            } else {
                itemView.background = itemView.resources.getDrawable(R.drawable.table_category_enable)
            }
        }
    }
}
