package com.itglance.restro_waiter.main.menu.order

enum class Status {
    ORDERED,COOKED,COOKING, SERVED, CLOSE, CANCELLED
}