package com.itglance.restro_waiter.main.tables

import java.util.UUID

class TableCategory {
    var id = UUID.randomUUID().toString()
    lateinit var name: String

    constructor()

    constructor(name: String){
        this.name = name
    }
}
