package com.itglance.restro_waiter.main

import android.app.Application
import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

import com.couchbase.lite.CouchbaseLiteException
import com.couchbase.lite.ResultSet
import com.couchbase.litecore.fleece.MContext
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.itglance.restro_waiter.DatabaseManager
import com.itglance.restro_waiter.Queries
import com.itglance.restro_waiter.main.menu.order.Order
import com.itglance.restro_waiter.main.menu.category.Category
import com.itglance.restro_waiter.main.menu.item.Item
import com.itglance.restro_waiter.main.menu.order.Status
import com.itglance.restro_waiter.main.tables.Group
import com.itglance.restro_waiter.main.tables.Table
import com.itglance.restro_waiter.main.tables.TableCategory
import com.itglance.restro_waiter.notification.NotificationHelper
import kotlin.collections.ArrayList

class MenuViewModel @Throws(CouchbaseLiteException::class)
constructor(application: Application) : AndroidViewModel(application) {
    private val objectMapper = ObjectMapper()
    val currentTable = MutableLiveData<Table>()
    val currentTableCategory = MutableLiveData<TableCategory>()

    val menuItems = MutableLiveData<List<Item>>()
    val categories = MutableLiveData<List<Category>>()
    val tableCategories = MutableLiveData<List<TableCategory>>()
    val tables = MutableLiveData<List<Table>>()
    val orders = MutableLiveData<List<Order>>()


    private var allTableCategories = ArrayList<TableCategory>()
    var allTables = ArrayList<Table>()
    private val allCategories = ArrayList<Category>()
    private val allItems = ArrayList<Item>()
    val allOrders = MutableLiveData<ArrayList<Order>>()
    val allGroups = MutableLiveData<ArrayList<Group>>()

    fun  setMenuCategory(category: Category?) {
        val menuItems = run {
            if (category!!.name.isBlank()) allItems else allItems.filter { menuItem -> category.id == menuItem.category }
        }
        this.menuItems.postValue(menuItems)
    }

    fun setCurrentTableCategory(tableCategory: TableCategory?) {
        val tables = run {
            if (tableCategory == null) allTables else allTables.filter { table -> tableCategory.id == table.category }
        }
        this.tables.postValue(tables)
    }

    fun updateGroupOrders() {
        val group = getCurrentGroup()

        val orders = run {
            allOrders.value?.filter { order -> order.group == group?.id }
        }?.sortedBy { it.time }?.reversed()
        this.orders.postValue(orders)
    }

    @Throws(TableNeedsTobeSelected::class)
    fun addGroup(): Group {
        val currentTable = currentTable.value ?: throw TableNeedsTobeSelected()

        val group = Group()
        group.name = currentTable.name
        group.table = currentTable.id
        Queries.getInstance(application = getApplication()).addGroup(group, objectMapper)
        return group
    }

    private fun getCurrentGroup(): Group? {
        val group = run {
            allGroups.value?.filter { group -> group.table == currentTable.value?.id }
        }?.sortedBy { it.time }
                ?.reversed()
                ?.firstOrNull()
        return group
    }

    @Throws(TableNeedsTobeSelected::class)
    fun addOrder(item: Item) {
        currentTable.value ?: throw TableNeedsTobeSelected()
        var group = getCurrentGroup()
        if (group == null) {
            group = addGroup()
        }

        val order = Order()
        order.group = group.id
        order.itemName = item.name!!
        order.item = item.id
        order.quantity = 1
        order.price = item.price
        order.status = Status.ORDERED.toString()
        Queries.getInstance(application = Application()).addOrders(order, objectMapper)
    }

    fun removeOrder(order: Order) {
        Queries.getInstance(getApplication()).updateOrderStatus(order, Status.CANCELLED.name)
    }

    fun setOrderAsServed(order: Order) {
        Queries.getInstance(getApplication()).updateOrderStatus(order, Status.SERVED.name)
    }



    class TableNeedsTobeSelected : Throwable() {

        val MESSAGE = "Please select a table first"
    }


    init {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        val queries = Queries.getInstance(application)

        val menuListQuery = queries.getMenu()
        menuListQuery.addChangeListener { change -> postMenuItems(change.results) }
        postMenuItems(menuListQuery.execute())

        val categoryQuery = queries.getMenuCategory()
        categoryQuery.addChangeListener { change -> postCategories(change.results) }
        postCategories(categoryQuery.execute())

        val tableCategoryListQuery = queries.getTableCategory()
        tableCategoryListQuery.addChangeListener { change -> postTableCategories(change.results) }
        postTableCategories(tableCategoryListQuery.execute())


        val tableListQuery = queries.getTable()
        tableListQuery.addChangeListener { change -> postTables(change.results) }
        postTableCategories(tableListQuery.execute())


        allOrders.value = ArrayList()
        val orderListQuery = queries.getOrders()
        orderListQuery.addChangeListener { change -> postOrders(change.results) }
        postOrders(orderListQuery.execute())


        allGroups.value = ArrayList()
        val groupListQuery = queries.getGroups()
        groupListQuery.addChangeListener { change -> postGroups(change.results) }
        postGroups(groupListQuery.execute())

        setCurrentTableCategory(null)
        orders.postValue(ArrayList())
        orders.value = allOrders.value
    }

    private fun postGroups(results: ResultSet) {
        val groups = ArrayList<Group>()
        for (categoryMap in results.allResults()) {
            val valueMap = categoryMap.getDictionary(DatabaseManager.getSharedInstance(getApplication()).database.name)
            val group = objectMapper.convertValue(valueMap.toMap(), Group::class.java)
            groups.add(group)
        }
        allGroups.value = groups

        updateGroupOrders()
    }

    private fun postTableCategories(categoryList: ResultSet) {
        allTableCategories.clear()
        for (categoryMap in categoryList.allResults()) {
            val valueMap = categoryMap.getDictionary(DatabaseManager.getSharedInstance(getApplication()).database.name)
            val category = objectMapper.convertValue(valueMap.toMap(), TableCategory::class.java)
            if (category?.name != null)
                allTableCategories.add(category)
        }
        tableCategories.postValue(allTableCategories)
    }

    private fun postMenuItems(menuList: ResultSet) {
        allItems.clear()
        for (menuItem in menuList.allResults()) {
            val valueMap = menuItem.getDictionary(DatabaseManager.getSharedInstance(getApplication()).database.name)
            val item = objectMapper.convertValue(valueMap.toMap(), Item::class.java)
            if (item?.name != null && item.size != null && item.price != null)
                allItems.add(item)
        }
        menuItems.postValue(allItems)
    }

    private fun postCategories(categories: ResultSet) {
        allCategories.clear()
        for (categoryResult in categories.allResults()) {
            val valueMap = categoryResult.getDictionary(DatabaseManager.getSharedInstance(getApplication()).database.name)
            val category = objectMapper.convertValue(valueMap.toMap(), Category::class.java)
            allCategories.add(category)
        }
        this.categories.postValue(allCategories)
    }

    private fun postTables(tableList: ResultSet) {
        allTables.clear()
        for (tableMap in tableList.allResults()) {
            val valueMap = tableMap.getDictionary(DatabaseManager.getSharedInstance(getApplication()).database.name)
            val table = objectMapper.convertValue(valueMap.toMap(), Table::class.java)
            if (table?.name != null)
                allTables.add(table)
        }
    }

    private fun postOrders(results: ResultSet) {
        val orders = ArrayList<Order>()
        for (orderMap in results.allResults()) {
            val valueMap = orderMap.getDictionary(DatabaseManager.getSharedInstance(getApplication()).database.name)
            val order = objectMapper.convertValue(valueMap.toMap(), Order::class.java)
            orders.add(order)
        }
        allOrders.value = orders
        updateGroupOrders()
    }
    private fun orderBills(results:ResultSet){
        val ordersBill = ArrayList<Order>()
        for (orderMap in results.allResults()){
            val valueMap = orderMap.getDictionary(DatabaseManager.getSharedInstance(getApplication()).database.name)
            val order = objectMapper.convertValue(valueMap.toMap(), Order::class.java)
            ordersBill.add(order)
        }
        orders.value = ordersBill
    }
    fun updateOrderStatus(order: Order,status: String,view: View) {


        Queries.getInstance(getApplication()).updateOrderStatus(order, status)
    }
    fun getGroupObject(id:String):Group? {
        var group = allGroups.value?.filter { group -> group.id == id }
        return if (group?.isNotEmpty()!!) {
            group?.get(0)
        } else {
            null
        }
    }

    companion object {
        private val TAG = "MenuViewModel"
    }

}
