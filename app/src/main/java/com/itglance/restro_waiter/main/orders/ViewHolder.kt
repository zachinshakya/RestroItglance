package com.itglance.restro_waiter.main.orders

import android.graphics.Color
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.getColor
import androidx.lifecycle.MutableLiveData
import butterknife.ButterKnife

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.main.menu.order.Order
import com.itglance.restro_waiter.main.menu.order.Status
import com.itglance.restro_waiter.main.tables.Table
import com.itglance.restro_waiter.notification.NotificationHelper
import java.util.concurrent.TimeUnit

class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

    internal val item = itemView.findViewById<TextView>(R.id.item_name_textView)
    private var quantity = itemView.findViewById<TextView>(R.id.item_quantity_textView)
    internal var table = itemView.findViewById<TextView>(R.id.table_name_textView)
    private var time = itemView.findViewById<TextView>(R.id.order_time_textView)
    private val start = itemView.findViewById<Button>(R.id.start_button)
    private val cooked = itemView.findViewById<Button>(R.id.cooked_button)
    private val serve = itemView.findViewById<TextView>(R.id.served_text_view)
    private val cookedText = itemView.findViewById<TextView>(R.id.cooked_text_view)
    private val cardView = itemView.findViewById<CardView>(R.id.all_order_card_view)

    init {
        ButterKnife.bind(this, itemView)
    }

    fun bind(order: Order) {
        quantity.text = order.quantity.toString()
        time.text = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - order.time).toString() + "m"
        start.tag = order
        when {
            order.status == Status.ORDERED.name -> {
                start.visibility = View.VISIBLE
                cooked.visibility = View.GONE
                cookedText.visibility = View.GONE
                serve.visibility = View.GONE
                start.tag = order
                cardView.setCardBackgroundColor(getColor(itemView.context, R.color.status_start))
                time.setTextColor(Color.RED)
            }
            order.status == Status.COOKING.name -> {
                start.visibility = View.GONE
                cooked.visibility = View.GONE
                cookedText.visibility = View.VISIBLE
                serve.visibility = View.GONE
                cooked.tag = order
                cardView.setCardBackgroundColor(getColor(itemView.context, R.color.status_cooking))
            }
            order.status == Status.COOKED.name -> {
                start.visibility = View.GONE
                cooked.visibility = View.VISIBLE
                cookedText.visibility = View.GONE
                serve.visibility = View.GONE
                cooked.tag = order
                cardView.setCardBackgroundColor(getColor(itemView.context, R.color.status_cooked))
            }
            order.status == Status.SERVED.name -> {
                start.visibility = View.GONE
                cooked.visibility = View.GONE
                cookedText.visibility = View.GONE
                serve.visibility = View.VISIBLE
                serve.tag = order
                cardView.setCardBackgroundColor(getColor(itemView.context, R.color.status_served))

            }
            else -> {
                start.visibility = View.GONE
                cooked.visibility = View.GONE
                serve.visibility = View.GONE
                cardView.setCardBackgroundColor(getColor(itemView.context, R.color.status_served))
            }
        }
    }
}