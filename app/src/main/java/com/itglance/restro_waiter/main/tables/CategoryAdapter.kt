package com.itglance.restro_waiter.main.tables

import androidx.lifecycle.MutableLiveData
import android.content.Context
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup

import com.itglance.restro_waiter.R

class CategoryAdapter(private val context: Context) : ListAdapter<TableCategory, CategoryViewHolder>(DIFF_CALLBACK) {

    lateinit var current: MutableLiveData<TableCategory>

    var selected = -1

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CategoryViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.table_category_recycler_view_layout, viewGroup, false)
        return CategoryViewHolder(view)
    }

    private fun setCurrent(adapterPosition: Int) {
        //set value so that appropriate values are read when setting selected
        current.value = getItem(adapterPosition)
        current.postValue(getItem(adapterPosition))
        notifyItemChanged(adapterPosition)
        notifyItemChanged(selected)
        selected = adapterPosition
    }

    override fun onBindViewHolder(viewHolder: CategoryViewHolder, i: Int) {
        viewHolder.name.text = getItem(i).name
        viewHolder.setAsSelected(getItem(i), current.value)
        viewHolder.itemView.setOnClickListener {
            setCurrent(i)
        }
    }

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TableCategory>() {
            override fun areItemsTheSame(oldItem: TableCategory, newItem: TableCategory): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: TableCategory, newItem: TableCategory): Boolean {
                return oldItem.name == newItem.name
            }
        }
    }
}
