package com.itglance.restro_waiter.main.orders

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife

import com.itglance.restro_waiter.main.MenuViewModel
import butterknife.Unbinder
import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.main.MainActivity
import com.itglance.restro_waiter.main.menu.order.Status

class OrderFragment : androidx.fragment.app.Fragment() {
    private lateinit var allOrderRecyclerView: RecyclerView
    private lateinit var newOrderRecyclerView: RecyclerView
    private lateinit var orderLayoutContainer: LinearLayout
    lateinit var unbinder: Unbinder
    lateinit var mainActivity: MainActivity
    private var orderStatus: String = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_all_order, container, false)
        unbinder = ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = MainActivity()
        orderLayoutContainer = view.findViewById(R.id.order_layout_container)
        newOrderRecyclerView = view.findViewById(R.id.new_order_recycler_view)
        allOrderRecyclerView = view.findViewById(R.id.all_order_recycler_view)

        val screenSize = this.context?.let { mainActivity.getScreenSize(it) }

        if (screenSize?.equals(SCREENSIZE.LARGE.name)!! || screenSize.equals(SCREENSIZE.XLARGE.name)) {

            orderLayoutContainer.orientation = LinearLayout.HORIZONTAL

            Log.d("SCREENSIZE", "screen $screenSize")
        } else {

            orderLayoutContainer.orientation = LinearLayout.VERTICAL

            Log.d("SCREENSIZE", "screen $screenSize")

        }

    }

    enum class SCREENSIZE {
        SMALL, NORMAL, LARGE, XLARGE
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val menu = ViewModelProviders.of(activity!!).get(MenuViewModel::class.java)
        context?.let {
            val adapter = OrderAdapter(it, menu)
            val orderAdapter = OrderAdapter(it, menu)


            newOrderRecyclerView.adapter = adapter
            allOrderRecyclerView.adapter = orderAdapter



            menu.allOrders.observe(this, Observer { orders ->
                adapter.submitList(orders!!.filter { order -> order.status != Status.CLOSE.name }.sortedBy { order -> order.time }.reversed())
            })

            menu.allOrders.observe(this, Observer { orders ->
                adapter.submitList(orders!!.filter { order -> order.status == Status.ORDERED.name }.sortedBy { order -> order.time }.reversed())
                                orderAdapter.submitList(orders.filter { order -> order.status != Status.ORDERED.name }.sortedBy { order -> order.status })
                allOrderRecyclerView.smoothScrollToPosition(0)
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }

}