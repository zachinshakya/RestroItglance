package com.itglance.restro_waiter.main.bill

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.itglance.restro_waiter.R

import com.itglance.restro_waiter.main.MenuViewModel
import com.itglance.restro_waiter.main.bill.order.OrderAdapter
import com.itglance.restro_waiter.utils.wheelPicker.LoopView
import java.util.ArrayList
import android.app.Activity
import android.app.AlertDialog
import android.app.Application
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.itglance.restro_waiter.dialog.BillDiscountDialog
import android.text.Editable
import android.util.Log
import android.view.*
import com.itglance.restro_waiter.main.MainActivity
import com.itglance.restro_waiter.main.menu.order.Order
import kotlinx.android.synthetic.main.layout_bill_details.*
import kotlin.math.floor


class BillFragment : Fragment() {

    private lateinit var menu: MenuViewModel
    private lateinit var orderView: RecyclerView
    private lateinit var subTotal: TextView
    private lateinit var tableName: TextView
    private lateinit var discountPicker: LoopView
    private lateinit var totalDiscount: EditText
    private lateinit var grandTotal: TextView
    lateinit var tenderAmount: EditText
    lateinit var returnAmountTextView: TextView

    var value: Int = 0
    private var grossAmount: Double = 0.0
    private var discountAmount: Double = 0.0
    private var tenderPrice: Double = 0.0
    var returnAmount: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bill, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        orderView = view.findViewById(R.id.orders_view)
        subTotal = view.findViewById(R.id.bill_subtotal)
        tableName = view.findViewById(R.id.bill_table_name)
        discountPicker = view.findViewById(R.id.discount_wheel_picker)
        totalDiscount = view.findViewById(R.id.bill_discount_edittext)
        grandTotal = view.findViewById(R.id.bill_grand_total_value)
        tenderAmount = view.findViewById(R.id.bill_tender)
        returnAmountTextView = view.findViewById(R.id.bill_change)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        menu = ViewModelProviders.of(activity!!).get(MenuViewModel::class.java)
        menu.currentTable.observe(this, Observer { table -> tableName.text = "${table?.name} (${menu.currentTableCategory.value?.name})" })
        setupOrders()
        setUpDiscount()
        menu.orders.observe(this, Observer { orders -> grossAmount = orders.sumByDouble { order -> order.price * order.quantity } })
        menu.orders.observe(this, Observer { subTotal.text = grossAmount.toString() })
        menu.orders.observe(this, Observer { grandTotal.text = subTotal.text })

        tenderAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                var gt: Double = floor(grossAmount.toInt().minus(discountAmount))
                try {
                    tenderPrice = java.lang.Double.parseDouble(tenderAmount.text.toString())
                    returnAmount = tenderPrice.minus(gt.toInt()).toInt()
                    returnAmountTextView.text = returnAmount.toString()
//                    }
                } catch (e: Exception) {
                    Toast.makeText(context, "Empty Tender", Toast.LENGTH_SHORT).show()
                    hideKeyboard(activity!!)
                }
            }
        })

        menu.currentTable.observe(this, Observer { table -> tenderAmount.text.clear() })
        menu.currentTable.observe(this, Observer { table -> returnAmountTextView.text = "" })

        //push view upward when soft keyboard open
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    private fun setupOrders() {
        context?.let {
            val adapter = OrderAdapter(it)
            menu.orders.observe(this, Observer(adapter::submitList))
            orderView.adapter = adapter
        }
    }

    private fun setUpDiscount() {
        val list = ArrayList<String>()
        for (i in 0..100) {
            list.add("$i")
        }
        discountPicker.setListener { discount ->
            menu.orders.observe(this, Observer { orders -> discountAmount = orders.sumByDouble { order -> order.price * order.quantity }?.times(discount)?.div(100) })
            menu.orders.observe(this, Observer { totalDiscount.setText(discountAmount.toString()) })

            menu.orders.observe(this, Observer { grandTotal.text = String.format("%.1f",grossAmount.minus(discountAmount)) })

        }
        discountPicker.setItems(list)
    }

    @Throws(MenuViewModel.TableNeedsTobeSelected::class)
    fun billPaid() {
        var gt = grossAmount.toInt().minus(discountAmount.toInt()).toInt()
        menu.currentTable.value ?: throw MenuViewModel.TableNeedsTobeSelected()
//        if (returnAmount < 0) {
//            val billDiscountDialog = BillDiscountDialog(context, returnAmount)
//            billDiscountDialog.show() }
        if (grossAmount == 0.0) {
            tenderAmount.text.clear()
            tenderAmount.error = "Add items first"
        } else if (gt==0 && tenderPrice == 0.0) {
            Toast.makeText(this.context, "Bill paid \"Thank You\" ", Toast.LENGTH_SHORT).show()
            paid_button.isEnabled = false
        } else if (tenderPrice < gt || tenderPrice < 0 || tenderPrice == 0.0) {
            tenderAmount.error = "Amount Invalid"
//                tenderAmount.text.clear()
        } else {
            Toast.makeText(this.context, "Bill paid \"Thank You\" ", Toast.LENGTH_SHORT).show()
            paid_button.isEnabled = false
        }
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}


