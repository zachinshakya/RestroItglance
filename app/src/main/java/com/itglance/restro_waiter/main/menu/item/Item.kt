package com.itglance.restro_waiter.main.menu.item

import java.util.*

open class Item {

    var id: String = UUID.randomUUID().toString()

    var name: String? = null

    var price: Double = 0.0
    var size: String? = null
    var category: String? = null

    @Suppress("unused")
    constructor()

    constructor(name: String, price: Double, size: String, category: String) {
        this.name = name
        this.price = price
        this.size = size
        this.category = category
    }
}
