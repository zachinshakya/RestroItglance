package com.itglance.restro_waiter.main.menu.category

import java.util.*

class Category {
    var id: String = UUID.randomUUID().toString()
    var name: String = ""

    constructor(name: String) {
        this.name = name
    }

    @Suppress("unused")
    constructor()
}
