package com.itglance.restro_waiter.main

import android.content.Context
import android.content.res.Configuration
import com.google.android.material.tabs.TabLayout
import androidx.multidex.MultiDex
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife


import com.facebook.stetho.Stetho
import com.google.firebase.messaging.FirebaseMessaging
import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.main.orders.OrderFragment
import com.itglance.restro_waiter.main.bill.BillFragment
import com.itglance.restro_waiter.main.menu.MenuFragment
import com.itglance.restro_waiter.main.menu.item.Item
import com.itglance.restro_waiter.main.menu.order.Order
import com.itglance.restro_waiter.main.menu.order.Status
import com.itglance.restro_waiter.main.tables.TableBottomSheet
import com.itglance.restro_waiter.notification.NotificationHelper
import com.itglance.restro_waiter.utils.SwipeHandler
import com.itglance.restro_waiter.utils.SwipeItemTouchHelperCallback
import kotlinx.android.synthetic.main.layout_bill_details.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private var viewPager: androidx.viewpager.widget.ViewPager? = null
    private var sliderAdapter: SliderAdapter? = null
    private var tabLayout: TabLayout? = null
    private lateinit var menu: MenuViewModel
    private var billFragment: BillFragment = BillFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        var helper=NotificationHelper(this)
        helper.subsribeToTopic()

        getView()
        Stetho.initializeWithDefaults(this)
        menu = ViewModelProviders.of(this).get(MenuViewModel::class.java)
        menu.currentTable.observe(this, Observer { menu.updateGroupOrders() })
        getScreenSize(this)

    }

    private fun getView() {
        viewPager = findViewById(R.id.main_view_pager)
        sliderAdapter = SliderAdapter(supportFragmentManager)
        tabLayout = findViewById(R.id.page_indicator_tab_layout)
        TableBottomSheet(this)
        setupFragments()
    }

    private fun setupFragments() {
        //Implement add to back stack
        sliderAdapter!!.addFragment(MenuFragment())
        sliderAdapter!!.addFragment(billFragment)
        sliderAdapter!!.addFragment(OrderFragment())
        viewPager!!.adapter = sliderAdapter
        viewPager!!.offscreenPageLimit = 3
        tabLayout!!.setupWithViewPager(viewPager)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    fun moveOrderStatus(view: View) {
        val order = view.tag as Order
        menu.updateOrderStatus(order,Status.values()[Status.valueOf(order.status).ordinal+1].name,view)
        var helper=NotificationHelper(this)

        when{
            Status.values()[Status.valueOf(order.status).ordinal].name == Status.COOKED.name -> {
              helper.sendNotification(view)
            }
        }
    }

    fun startNewGroup(view: View) {
        try {
            menu.addGroup()
            menu.updateGroupOrders()
            menu.currentTable.observe(this, Observer { table -> billFragment.tenderAmount.text.clear() })
            menu.currentTable.observe(this, Observer { table -> billFragment.returnAmountTextView.text = "" })
            paid_button.isEnabled=true
        } catch (e: MenuViewModel.TableNeedsTobeSelected) {
            Toast.makeText(this, e.MESSAGE, Toast.LENGTH_LONG).show()
        }
    }

    fun payBill(view: View){
        try{
            billFragment.billPaid()
        }catch (e:MenuViewModel.TableNeedsTobeSelected){
            Toast.makeText(this,e.MESSAGE,Toast.LENGTH_SHORT).show()
        }
    }


    fun addOrder(view: View) {
        try {
            val item = view.tag as Item
            menu.addOrder(item)
        } catch (tableNotSelected: MenuViewModel.TableNeedsTobeSelected) {
            Toast.makeText(this, tableNotSelected.MESSAGE, Toast.LENGTH_SHORT).show()
        }
    }

    fun removeOrder(view: View) {
        try {
            val order = view.tag as Order
            Toast.makeText(this, order.itemName +"removed", Toast.LENGTH_SHORT).show()
        } catch (tableNotSelected: MenuViewModel.TableNeedsTobeSelected) {
            Toast.makeText(this, tableNotSelected.MESSAGE, Toast.LENGTH_SHORT).show()
        }
    }

    fun getScreenSize(context: Context): String{
        var screenLayout = context.resources.configuration.screenLayout
        screenLayout = screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK

        return when (screenLayout) {
            Configuration.SCREENLAYOUT_SIZE_SMALL -> "SMALL"
            Configuration.SCREENLAYOUT_SIZE_NORMAL -> "NORMAL"
            Configuration.SCREENLAYOUT_SIZE_LARGE -> "LARGE"
            Configuration.SCREENLAYOUT_SIZE_XLARGE-> "XLARGE"
            else -> "undefined"
        }
    }


}