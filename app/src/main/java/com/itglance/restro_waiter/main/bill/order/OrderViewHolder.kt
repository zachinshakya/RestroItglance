package com.itglance.restro_waiter.main.bill.order

import android.view.View
import android.widget.TextView

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.main.menu.order.Order
import java.util.concurrent.TimeUnit

class OrderViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
    var sn: TextView = itemView.findViewById(R.id.bill_item_sn)
    var name: TextView = itemView.findViewById(R.id.bill_item_name)
    private var quantity: TextView = itemView.findViewById(R.id.bill_item_quantity)
    private var price: TextView = itemView.findViewById(R.id.bill_item_price)
    private var itemTotal: TextView = itemView.findViewById(R.id.bill_item_total)

    fun bind(order: Order) {
        sn.text = (adapterPosition + 1).toString()
        name.text = order.itemName
        quantity.text = order.quantity.toString()
        price.text = order.price.toString()
        itemTotal.text = (order.price * order.quantity).toString()
    }
}
