package com.itglance.restro_waiter.main.menu.category

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.common.ItemSelectedListener

class CategoryViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

    private val menuCategoryTextView: TextView = itemView.findViewById(R.id.menu_category_textView)

    fun bind(category: Category, selectedListener: ItemSelectedListener, currentCategory: Category?) {
        menuCategoryTextView.text = category.name
        itemView.setOnClickListener {
            selectedListener(adapterPosition)
        }
        setAsSelected(category, currentCategory)
    }

    private fun setAsSelected(category: Category, currentCategory: Category?) {
        currentCategory?.let {
            if (it.id == category.id) {
                itemView.background = itemView.resources.getDrawable(R.drawable.table_category_pressed)
                menuCategoryTextView.setTextColor(itemView.resources.getColor(R.color.cardColor))
            } else {
                itemView.background = itemView.resources.getDrawable(android.R.color.transparent)
                menuCategoryTextView.setTextColor(itemView.resources.getColor(R.color.descriptionTextColor))
            }
        }
    }
}