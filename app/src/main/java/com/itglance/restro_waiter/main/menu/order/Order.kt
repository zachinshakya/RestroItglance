package com.itglance.restro_waiter.main.menu.order

import java.util.*

class Order {
    var status: String = Status.ORDERED.name
    lateinit var item: String
    lateinit var itemName: String
    var id: String = UUID.randomUUID().toString()
    var description: String = ""
    var quantity: Int = 0
    var time: Long = System.currentTimeMillis()
    var price: Double = 0.0
    lateinit var group: String

    @SuppressWarnings("unused")
    constructor()

    constructor(item: String, group: String, description: String, quantity: Int, time: Long, status: String, price: Double) {
        this.item = item
        this.group = group
        this.description = description
        this.quantity = quantity
        this.time = time
        this.status = status
        this.price = price
    }
}
