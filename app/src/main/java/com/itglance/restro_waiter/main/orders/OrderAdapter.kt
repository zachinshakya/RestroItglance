package com.itglance.restro_waiter.main.orders

import android.content.Context
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData

import com.itglance.restro_waiter.R
import com.itglance.restro_waiter.main.MenuViewModel
import com.itglance.restro_waiter.main.menu.item.Item
import com.itglance.restro_waiter.main.menu.order.Order
import com.itglance.restro_waiter.main.tables.Table

class OrderAdapter(private val context: Context, private val menu: MenuViewModel) : ListAdapter<Order, ViewHolder>(DIFF_CALLBACK) {


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.all_order_recycler_view_layout_design, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.bind(getItem(i))
        viewHolder.item.text = getItem(i).itemName
        try {
            viewHolder.table.text = menu.allGroups.value?.single { group -> group.id == getItem(i).group }?.name
        } catch (e: NoSuchElementException) {
            viewHolder.table.text = "Loading..."
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Order>() {
            override fun areItemsTheSame(oldItem: Order, newItem: Order): Boolean {
                return oldItem.id == newItem.id && oldItem.status == newItem.status
            }

            override fun areContentsTheSame(oldItem: Order, newItem: Order): Boolean {
                return oldItem.item == newItem.item && oldItem.status == newItem.status
            }
        }
    }


}