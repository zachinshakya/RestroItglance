package com.itglance.restro_waiter.main.tables

enum class Status { OPEN, RESERVED }