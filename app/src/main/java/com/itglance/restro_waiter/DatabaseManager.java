package com.itglance.restro_waiter;

import android.content.Context;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.DatabaseConfiguration;
import com.couchbase.lite.Endpoint;
import com.couchbase.lite.Replicator;
import com.couchbase.lite.ReplicatorConfiguration;
import com.couchbase.lite.URLEndpoint;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

public class DatabaseManager {

    private final static String DATABASE_NAME = "university";

    public Database database;
    private Replicator replicator;

    private static DatabaseManager instance = null;


    private DatabaseManager(Context context) {

        try {

            DatabaseConfiguration config = new DatabaseConfiguration(context);
            File dir = context.getDir("CBL", Context.MODE_PRIVATE);
            config.setDirectory(dir.toString());
            database = new Database(DATABASE_NAME, config);


        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }

    }

    private DatabaseManager(Context context, String DATABASE_NAME) {

        try {

            DatabaseConfiguration config = new DatabaseConfiguration(context);
            File dir = context.getDir("CBL", Context.MODE_PRIVATE);
            config.setDirectory(dir.toString());
            database = new Database(DATABASE_NAME, config);


        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }

    }

    public static DatabaseManager getSharedInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseManager(context);
            startReplication(instance.database);
        }
        return instance;
    }

    private static void startReplication(Database database) {
        URI uri = null;
        try {
            uri = new URI("ws://192.168.88.221:4984/restrobytes/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Endpoint endpoint = new URLEndpoint(uri);
        ReplicatorConfiguration config = new ReplicatorConfiguration(database, endpoint);
        config.setReplicatorType(ReplicatorConfiguration.ReplicatorType.PUSH_AND_PULL);
        config.setContinuous(true);
        instance.replicator = new Replicator(config);
        instance.replicator.resetCheckpoint();
        instance.replicator.start();
    }
}

