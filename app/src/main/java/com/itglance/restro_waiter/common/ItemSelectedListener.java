package com.itglance.restro_waiter.common;

public interface ItemSelectedListener {
    void invoke(int adapterPosition);
}
