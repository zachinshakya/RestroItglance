package com.itglance.restro_waiter.common

import com.fasterxml.jackson.annotation.JsonIgnore

open class UiStates {

    @JsonIgnore
    var selected: Boolean = false

    @JsonIgnore
    var itemId: Int = -1
}
