package com.itglance.restro_waiter.common

interface ChoiceListener<T> {
    operator fun invoke(obj: T)
}