package com.itglance.restro_waiter

import android.app.Application
import android.util.Log
import com.couchbase.lite.*
import com.fasterxml.jackson.databind.ObjectMapper

import com.itglance.restro_waiter.main.menu.order.Order
import com.itglance.restro_waiter.main.tables.Group
import java.util.HashMap

class Queries private constructor(val application: Application) {


    init {
        Log.d(TAG, "Queries: here")
    }


    internal fun getMenu(): Query {
        val dbMgr = DatabaseManager.getSharedInstance(application)
        return QueryBuilder.select(SelectResult.all()).from(DataSource.database(dbMgr.database)).where(Expression.property("type").equalTo(Expression.string("menu_item")))
    }

    internal fun getMenuCategory(): Query {
        val dbMgr = DatabaseManager.getSharedInstance(application)
        return QueryBuilder.select(SelectResult.all()).from(DataSource.database(dbMgr.database)).where(Expression.property("type").equalTo(Expression.string("category")))
    }

    internal fun getTable(): Query {
        val dbMgr = DatabaseManager.getSharedInstance(application)
        return QueryBuilder.select(SelectResult.all()).from(DataSource.database(dbMgr.database)).where(Expression.property("type").equalTo(Expression.string("table")))
    }

    internal fun getTableCategory(): Query {
        val dbMgr = DatabaseManager.getSharedInstance(application)
        return QueryBuilder.select(SelectResult.all()).from(DataSource.database(dbMgr.database)).where(Expression.property("type").equalTo(Expression.string("table_category")))
    }

    fun getOrders(): Query {
        val dbMgr = DatabaseManager.getSharedInstance(application)
        return QueryBuilder.select(SelectResult.all()).from(DataSource.database(dbMgr.database)).where(Expression.property("type").equalTo(Expression.string("order")).and(Expression.property("status").isNot(Expression.string("CANCELLED"))))
    }

    fun orders(order: Order, objectMapper: ObjectMapper) {
        val dbMgr = DatabaseManager.getSharedInstance(application)
        try {
            val doc = MutableDocument(objectMapper.convertValue(order, HashMap::class.java) as HashMap<String, Any>)
            doc.setString("type", "orders")
            dbMgr.database.save(doc)
        } catch (e: CouchbaseLiteException) {
            e.printStackTrace()
        }
    }


    fun addOrders(order: Order, objectMapper: ObjectMapper) {
        val dbMgr = DatabaseManager.getSharedInstance(application)
        try {
            val doc = MutableDocument(objectMapper.convertValue(order, HashMap::class.java) as HashMap<String, Any>)
            doc.setString("type", "order")
            dbMgr.database.save(doc)
        } catch (e: CouchbaseLiteException) {
            e.printStackTrace()
        }
    }

    fun addGroup(group: Group, objectMapper: ObjectMapper) {
        val dbMgr = DatabaseManager.getSharedInstance(application)
        try {
            val doc = MutableDocument(objectMapper.convertValue(group, HashMap::class.java) as HashMap<String, Any>)
            doc.setString("type", "group")
            dbMgr.database.save(doc)
        } catch (e: CouchbaseLiteException) {
            e.printStackTrace()
        }
    }

    fun updateOrderStatus(order: Order, status: String) {
        val dbMgr = DatabaseManager.getSharedInstance(application)
        try {
            val query = QueryBuilder.select(SelectResult.expression(Meta.id).`as`("meta_id")).from(DataSource.database(dbMgr.database)).where(Expression.property("type").equalTo(Expression.string("order")).and(Expression.property("id").equalTo(Expression.string(order.id))))
            val document = dbMgr.database.getDocument(query.execute().next().toMap()["meta_id"] as String)
            dbMgr.database.save(document.toMutable().setString("status", status))
        } catch (e: CouchbaseLiteException) {
            e.printStackTrace()
        }
    }

    fun getGroups(): Query {
        val dbMgr = DatabaseManager.getSharedInstance(application)
        return QueryBuilder.select(SelectResult.all()).from(DataSource.database(dbMgr.database)).where(Expression.property("type").equalTo(Expression.string("group")))
    }

    companion object {
        private val TAG = "Queries"
        private var instance: Queries? = null

        internal fun getInstance(application: Application): Queries {
            if (instance != null) {
                return instance as Queries
            }
            instance = Queries(application)
            return instance as Queries
        }
    }

}
