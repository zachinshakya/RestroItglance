package com.itglance.restro_waiter

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.couchbase.lite.CouchbaseLiteException
import com.couchbase.lite.MutableDocument
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.itglance.restro_waiter.main.MainActivity
import com.itglance.restro_waiter.main.menu.category.Category
import com.itglance.restro_waiter.main.menu.item.Item
import com.itglance.restro_waiter.main.tables.Status
import com.itglance.restro_waiter.main.tables.Table
import java.util.ArrayList
import java.util.HashMap

class SplashScreenActivity : AppCompatActivity() {

    private var handler: Handler = Handler()
    private val splashDisplayLength: Long = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        val imageView = findViewById<ImageView>(R.id.splash_image)
        Glide.with(this).load(R.drawable.logo).into(imageView)
        afterUserLoggedIn()
    }

    private fun afterUserLoggedIn() {
        val result : Boolean = isFirstTime()
        if(result){
            setMenuItemData()
            setTableData()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        else{
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    private var firstTime: Boolean? = null
    private fun isFirstTime(): Boolean {
        if (firstTime == null) {
            val mPreferences = this.getSharedPreferences("first_time", Context.MODE_PRIVATE)
            firstTime = mPreferences.getBoolean("firstTime", false)
            if (firstTime!!) {
                val editor = mPreferences.edit()
                editor.putBoolean("firstTime", false)
                editor.commit()
            }
        }
        return firstTime!!
    }

    internal fun setTableData() {
        val categories = ArrayList<com.itglance.restro_waiter.main.tables.TableCategory>()
        categories.add(com.itglance.restro_waiter.main.tables.TableCategory("Ground Floor"))
        categories.add(com.itglance.restro_waiter.main.tables.TableCategory("1st Floor"))
        categories.add(com.itglance.restro_waiter.main.tables.TableCategory("2nd Floor"))

        val tables = ArrayList<Table>()
        tables.add(Table("Table 1", categories[0].id, Status.OPEN))
        tables.add(Table("Table 2", categories[0].id, Status.OPEN))
        tables.add(Table("Table 3", categories[0].id, Status.OPEN))
        tables.add(Table("Table 4", categories[0].id, Status.OPEN))
        tables.add(Table("Table 5", categories[0].id, Status.OPEN))
        tables.add(Table("Table 6", categories[0].id, Status.OPEN))
        tables.add(Table("Table 7", categories[0].id, Status.OPEN))
        tables.add(Table("Table 8", categories[0].id, Status.OPEN))
        tables.add(Table("Table 9", categories[0].id, Status.OPEN))
        tables.add(Table("Table 10", categories[0].id, Status.OPEN))

        tables.add(Table("Table 11", categories[1].id, Status.OPEN))
        tables.add(Table("Table 12", categories[1].id, Status.OPEN))
        tables.add(Table("Table 13", categories[1].id, Status.OPEN))
        tables.add(Table("Table 14", categories[1].id, Status.OPEN))
        tables.add(Table("Table 15", categories[1].id, Status.OPEN))
        tables.add(Table("Table 16", categories[1].id, Status.OPEN))
        tables.add(Table("Table 17", categories[1].id, Status.OPEN))


        val dbmgr = DatabaseManager.getSharedInstance(application)
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        for (category in categories) {
            try {
                val categoryMaps = MutableDocument(objectMapper.convertValue(category, HashMap::class.java) as HashMap<String, Any>)
                categoryMaps.setString("type", "table_category")
                dbmgr.database.save(categoryMaps)
            } catch (e: CouchbaseLiteException) {
                e.printStackTrace()
            }

        }
        for (table in tables) {
            try {
                val tableMaps = MutableDocument(objectMapper.convertValue(table, HashMap::class.java) as HashMap<String, Any>)
                tableMaps.setString("type", "table")
                dbmgr.database.save(tableMaps)
            } catch (e: CouchbaseLiteException) {
                e.printStackTrace()
            }

        }
    }

    internal fun setMenuItemData() {

        val menuCategories = ArrayList<Category>()

        menuCategories.add(Category("Regular Burger"))
        menuCategories.add(Category("Burger"))
        val chicken = Category("Crunchy Fried Chicken")
        menuCategories.add(chicken)
        menuCategories.add(Category("Hot dog"))
        menuCategories.add(Category("Sandwich"))
        menuCategories.add(Category("Soup"))
        menuCategories.add(Category("Stick Food"))
        val momo = Category("Mo: Mo:")
        menuCategories.add(momo)
        menuCategories.add(Category("Chowmein/Thukpa"))
        menuCategories.add(Category("Fried Rice"))
        menuCategories.add(Category("Spring Roll"))
        menuCategories.add(Category("Wrap"))
        menuCategories.add(Category("Choupsey"))
        menuCategories.add(Category("Pasta"))
        menuCategories.add(Category("Pizza"))

        val testMenuItem = ArrayList<Item>()
        testMenuItem.add(Item("Buff C.MOMO", 120.0, "large", momo.id))
        testMenuItem.add(Item("Chicken C.MOMO", 120.0, "large", momo.id))
        testMenuItem.add(Item("Veg C.MOMO", 120.0, "large", momo.id))
        testMenuItem.add(Item("BoneLess Chicken Chilly", 120.0, "large", chicken.id))
        testMenuItem.add(Item("Chicken Pan Pizza", 120.0, "large", chicken.id))
        testMenuItem.add(Item("Chicken Fried Pan Pizza", 120.0, "large", chicken.id))

        val dbmgr = DatabaseManager.getSharedInstance(application)
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        for (category in menuCategories) {
            try {
                val categoryDocs = MutableDocument(objectMapper.convertValue(category, HashMap::class.java) as HashMap<String, Any>)
                categoryDocs.setString("type", "category")
                dbmgr.database.save(categoryDocs)
            } catch (e: CouchbaseLiteException) {
                e.printStackTrace()
            }

        }
        for (menuItem in testMenuItem) {
            try {
                val menuItemDocs = MutableDocument(objectMapper.convertValue(menuItem, HashMap::class.java) as HashMap<String, Any>)
                menuItemDocs.setString("type", "menu_item")
                dbmgr.database.save(menuItemDocs)
            } catch (e: CouchbaseLiteException) {
                e.printStackTrace()
            }

        }

    }
}

