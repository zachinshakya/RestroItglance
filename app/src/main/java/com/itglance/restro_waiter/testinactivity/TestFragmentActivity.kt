package com.itglance.restro_waiter.testinactivity

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.itglance.restro_waiter.R

class TestFragmentActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_fragment)
    }
}
