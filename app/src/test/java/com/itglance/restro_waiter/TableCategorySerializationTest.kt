package com.itglance.restro_waiter

import com.fasterxml.jackson.databind.ObjectMapper
import com.itglance.restro_waiter.main.menu.category.Category
import org.junit.Test
import java.util.HashMap

class TableCategorySerializationTest {

    @Test
    fun testItemSerialization() {
        val category = Category("somename")
        val objectMapper = ObjectMapper()
        val convertValue = objectMapper.convertValue(category, HashMap::class.java)
        println(convertValue)
    }
}
