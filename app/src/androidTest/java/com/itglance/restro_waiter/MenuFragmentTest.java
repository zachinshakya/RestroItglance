//package com.itglance.restro_waiter;
//
//import androidx.fragment.app.Fragment;
//import androidx.fragment.app.FragmentTransaction;
//import androidx.test.filters.SmallTest;
//import androidx.test.rule.ActivityTestRule;
//import androidx.test.runner.AndroidJUnit4;
//
//import com.itglance.restro_waiter.testinactivity.TestFragmentActivity;
//
//import org.junit.runner.RunWith;
//
//import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
//
//@SmallTest
//@RunWith(AndroidJUnit4.class)
//public class MenuFragmentTest extends ActivityTestRule<TestFragmentActivity> {
//
//    private TestFragmentActivity testFragmentActivity;
//    public MenuFragmentTest() {
//        super(TestFragmentActivity.class);
//    }
//
//    @Override
//    protected void setUp() throws Exception {
//        super.setUp();
//        mActivity = getActivity();
//    }
//
//    private Fragment startFragment(Fragment fragment) {
//        FragmentTransaction transaction = testFragmentActivity.getSupportFragmentManager().beginTransaction();
//        transaction.add(R.id.activity_test_fragment_linearlayout, fragment, "tag");
//        transaction.commit();
//        getInstrumentation().waitForIdleSync();
//        Fragment frag = testFragmentActivity.getSupportFragmentManager().findFragmentByTag("tag");
//        return frag;
//    }
//
//    public void testFragment() {
//        MenuFragmentTest fragment = new MenuFragmentTest() {
//            //Override methods and add assertations here.
//        };
//
//        Fragment frag = startFragment(fragment);
//    }
//}