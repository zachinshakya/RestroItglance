//package com.itglance.restro_waiter;
//
//import android.content.Intent;
//import androidx.test.filters.SmallTest;
//import androidx.test.runner.AndroidJUnit4;
//
//import com.itglance.restro_waiter.login.LoginActivity;
//
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import androidx.test.rule.ActivityTestRule;
//
//import static androidx.test.espresso.Espresso.onView;
//import static androidx.test.espresso.action.ViewActions.click;
//import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
//import static androidx.test.espresso.action.ViewActions.typeText;
//import static androidx.test.espresso.matcher.ViewMatchers.withId;
//
//@SmallTest
//@RunWith(AndroidJUnit4.class)
//public class LoginValidatorTest {
//
//    @Rule
//    public ActivityTestRule<LoginActivity> mActivityRule
//            = new ActivityTestRule<>(LoginActivity.class);
//
//    @Test
//    public void sucessfulLogin(){
//
//        onView(withId(R.id.admin_username_edit_text))
//                .perform(typeText("kitchen"), closeSoftKeyboard());
//
//        onView(withId(R.id.admin_password_edit_text))
//                .perform(typeText("user"),closeSoftKeyboard());
//        onView(withId(R.id.login_button)).perform(click());
//
//        mActivityRule.launchActivity(new Intent());
//    }
//}
